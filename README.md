# NodeRestreamer

NodeRestreamer è un'app che replica un flusso in entrata su più piattaforme in uscita (mediante RTMP), compreso il dispositivo locale. L'integrazione con la libreria node-media-server su GitHub (https://github.com/illuspas/Node-Media-Server, [licenza MIT](https://github.com/illuspas/Node-Media-Server/blob/master/LICENSE)). L'applicativo funziona su dispositivi con Windows 10, MacOS e Linux.

## Primo Utilizzo

Soltanto al primo utilizzo è necessario:
 1. Installare NodeJS scaricandolo da [qui](https://nodejs.dev/download).
 2. Scaricare l'archivio zip contenente tutti i file sorgente dell'applicativo (cliccare sul pulsante di download della repository presente in questa schermata accanto al pulsante "Clone").
 3. Scaricare e installare ffmpeg.
 4. Modificare il file `index.js` con il percorso in cui si trova il file `ffmpeg`.
 5. Rendere eseguibile il file `launch` cliccandoci sopra con il tasto destro, aprire `Proprietà`, poi `Permessi` e poi abilitare l'`esecuzione` del file.

I punti 2,4 e 5 vanno ripetuti ogni qualvolta che l'applicativo viene aggiornato.

## Utilizzo dell'applicativo

Terminata la fase di preparazione all'utilizzo, ogni volta che si desidera utilizzare l'applicativo si deve:

 1. Eseguire il setup dell'infrastruttura, connettendo il PC su cui gira il software per la gestione della diretta (tipicamente OBS, vMix, MimoLive, ...) e PC sul quale gira NodeRestreamer nella stessa rete.
 2. Aprire con un editor di testo (Gedit, TextEdit, Blocco Note, VS Code, ...) il file `.env` presente nella cartella principale dell'applicativo e modificare con i valori dei link per lo streaming e relative chiavi per lo stream, poi salvare e chiudere.
 4. Cliccare sull'icona `launch` per lanciare l'applicativo.
 5. Semmai il punto 4 non dovesse funzionare, per eseguire l'applicativo occorre:
    - Aprire il Terminale
    - Entrare nella cartella NodeRestreamer, utilizzando il comando `cd` per scorrere tra le directory e `ls` per visualizzare il contenuto della cartella ([video Tutorial](https://youtu.be/cPeC-GacLNA)).
    - Una volta entrati nella cartella, digitare `node index.js` e attendere l'avvio dell'applicativo.
 5. Verrà aperta una finestra di Terminale contenente messaggi di avvenuta esecuzione e eventuali errori.

## Chiusura dell'applicativo

Per chiudere l'applicativo è sufficiente premere la combinazione di tasti `CTRL+C` su Windows o `CMD+C` nel Terminale, poi chiuderlo.

## Credits

 - https://github.com/illuspas/Node-Media-Server
 - luca@alpad.it
