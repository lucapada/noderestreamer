const NodeMediaServer = require('node-media-server');
const dotenv = require('dotenv').config();

var tasks = [];

if(process.env.YOUTUBE_STREAM_URL != "" && process.env.YOUTUBE_STREAM_KEY != ""){
  console.log("YouTube Impostato");
  tasks.push({
    app: 'live',
    mode: 'push',
    edge: process.env.YOUTUBE_STREAM_URL + '/' + process.env.YOUTUBE_STREAM_KEY,
    appendName: false
  });
}

if(process.env.FACEBOOK_EDGE != ""){
  console.log("FaceBook Impostato");
  tasks.push({
    app: 'live',
    mode: 'push',
    edge: process.env.FACEBOOK_EDGE,
    appendName: false
  });
}

const config = {
  rtmp: {
    port: 1935,
    chunk_size: 60000,
    gop_cache: true,
    ping: 30,
    ping_timeout: 60
  },
  http: {
    port: 8000,
    allow_origin: '*'
  },
  /*auth: {
    play: true,
    publish: true,
    secret: process.env.STREAMING_SECRET
  },*/
  relay: {
    ffmpeg: '/usr/bin/ffmpeg',
    tasks: tasks
  }  
};

var nms = new NodeMediaServer(config)
nms.run();

/*nms.on('preConnect', (id, args) => {
  console.log('[NodeEvent on preConnect]', `id=${id} args=${JSON.stringify(args)}`);
  // let session = nms.getSession(id);
  // session.reject();
});

nms.on('postConnect', (id, args) => {
  console.log('[NodeEvent on postConnect]', `id=${id} args=${JSON.stringify(args)}`);
});

nms.on('doneConnect', (id, args) => {
  console.log('[NodeEvent on doneConnect]', `id=${id} args=${JSON.stringify(args)}`);
});

nms.on('prePublish', (id, StreamPath, args) => {
  console.log('[NodeEvent on prePublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
  // let session = nms.getSession(id);
  // session.reject();
});

nms.on('postPublish', (id, StreamPath, args) => {
  console.log('[NodeEvent on postPublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
});

nms.on('donePublish', (id, StreamPath, args) => {
  console.log('[NodeEvent on donePublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
});

nms.on('prePlay', (id, StreamPath, args) => {
  console.log('[NodeEvent on prePlay]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
  // let session = nms.getSession(id);
  // session.reject();
});

nms.on('postPlay', (id, StreamPath, args) => {
  console.log('[NodeEvent on postPlay]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
});

nms.on('donePlay', (id, StreamPath, args) => {
  console.log('[NodeEvent on donePlay]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
});*/